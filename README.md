# Frontend Developer Exercise

## Requirements

- Node.js and npm (you'll get both installing [Node.js](https://nodejs.org/))
- Both [Bower](https://bower.io/) and [Bower Installer](https://github.com/rquadling/bower-installer) installed globally

##### To run project locally you will need to do the following:

```
$ npm install -g bower (if not already installed)

$ npm install -g bower-installer (if not already installed)

$ npm install

$ bower-installer
```

##### To start the project, just run the command:

```
$ npm run build
$ npm start
```

## Tests

##### To run tests it is a good idea to have karma-cli installed globally:

```
$ npm install -g karma-cli
```

##### With karma-cli installed, just start karma:

```
$ karma start
```
