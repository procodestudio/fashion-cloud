var browserSync = require('browser-sync').create();
var concat = require('gulp-concat');
var config = require('./gulp.config')();
var entity = require('gulp-entity-convert');
var gulp = require('gulp');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var pug = require('gulp-pug');
var stylus = require('gulp-stylus');
var jeet = require('jeet');
var axis = require('axis');
var rupture = require('rupture');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');
var minify = require('gulp-clean-css');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var historyApiFallback = require('connect-history-api-fallback');
var outputDir = config.dist;
var srcDir = config.src;
var onError;

onError = function(err) {
  notify.onError({
    title: "Gulp Task Error",
    subtitle: "Failure!",
    message: "Error: <%= error.message %>",
    sound: "Beep"
  })(err);

  this.emit('end');
};

gulp.task('js', function() {
  return gulp
    .src([
      '!' + srcDir + 'js/**/*.spec.js',
      srcDir + 'js/app.js',
      srcDir + 'js/common/constants.js',
      srcDir + 'js/**/*.js'
    ])
    .pipe(plumber({errorHandler: onError}))
    .pipe(uglify())
    .pipe(concat('app.js'))
    .pipe(gulp.dest(outputDir + 'js'))
    .pipe(notify('JS files was compiled to ' + outputDir))
    .pipe(browserSync.stream())
});

gulp.task('3rd', function() {
  return gulp
    .src([
      srcDir + 'lib/jquery/jquery.js',
      srcDir + 'lib/angular/angular.js',
      srcDir + 'lib/angular-ui-router/angular-ui-router.js',
      srcDir + 'lib/lodash/lodash.min.js',
      srcDir + 'lib/moment/moment.js',
      srcDir + 'lib/popper.js/popper.js',
      srcDir + 'lib/**/*.js'
    ])
    .pipe(plumber({errorHandler: onError}))
    .pipe(uglify())
    .pipe(concat('3rd.js'))
    .pipe(gulp.dest(outputDir + 'js'))
    .pipe(notify('3rd JS files was compiled to ' + outputDir))
    .pipe(browserSync.stream())
});

gulp.task('pug', function() {
  return gulp
    .src([srcDir + '**/*.pug', srcDir + '!(_)**/*.pug'])
    .pipe(plumber({errorHandler: onError}))
    .pipe(pug({pretty: '\t'}))
    .pipe(entity())
    .pipe(gulp.dest(outputDir))
    .pipe(notify('Pug files was compiled to ' + outputDir))
    .pipe(browserSync.stream())
});

gulp.task('stylus', function() {
  return gulp
    .src(srcDir + 'stylus/main.styl')
    .pipe(plumber({errorHandler: onError}))
    .pipe(sourcemaps.init())
    .pipe(stylus({use: [jeet(), axis(), rupture()]}))
    .pipe(autoprefixer({browsers: ['last 2 version', 'safari 5', 'ie 6', 'ie 7', 'ie 8', 'ie 9', 'opera 12.1']}))
    .pipe(minify())
    .pipe(rename({ suffix: '.min' }))
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(outputDir + 'css'))
    .pipe(notify('Stylus files was compiled to ' + outputDir))
    .pipe(browserSync.stream())
});

gulp.task('browser-sync', function() {
  var files = [
    outputDir + '**/*.html',
    outputDir + 'css/**/*.css',
    outputDir + 'js/**/*.js'
  ];

  browserSync.init(files, {
    server: {
      baseDir: outputDir,
      middleware: [historyApiFallback()]
    }
  });
});

gulp.task('watch', function() {
  gulp.watch(srcDir + '**/**/*', {cwd:'./'}, ['pug']);
  gulp.watch([srcDir + 'js/**/*.js', '!' + srcDir + 'js/**/*.spec.js'], {cwd:'./'}, ['js']);
  gulp.watch(srcDir + 'stylus/*.styl', {cwd:'./'}, ['stylus']);
});

gulp.task('default', ['pug', 'js', '3rd', 'stylus', 'watch']);
gulp.task('bs', ['browser-sync']);
