module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],
    files: [
      './bower_components/moment/moment.js',
      './bower_components/jquery/dist/jquery.js',
      './bower_components/angular/angular.js',
      './node_modules/angular-mocks/angular-mocks.js',
      './bower_components/angular-ui-router/release/angular-ui-router.js',
      './bower_components/lodash/dist/lodash.js',
      './dist/templates/*.html',
      './src/js/**/*.js',
      './src/js/**/*.spec.js'
    ],
    exclude: [],
    preprocessors: {
      './src/js/**/*.js': ['coverage'],
      './dist/templates/*.html': ['ng-html2js']
    },
    ngHtml2JsPreprocessor: {
      moduleName: 'templates',
      stripPrefix: 'dist/'
    },
    reporters: ['coverage', 'spec'],
    coverageReporter: {
      reporters: [{
        type: 'text-summary'
      },{
        type : 'html',
        dir : 'coverage/'
      }
      ]
    },
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['PhantomJS'],
    singleRun: false,
    concurrency: Infinity
  })
};
