describe('MainController', function() {
  var MainController;

  beforeEach(function() {
    module('app');

    inject(function($injector, $controller) {
      MainController = $controller('MainController');
    });
  });

  it('controller should exists', function() {
    expect(MainController).toBeDefined();
  });
});
