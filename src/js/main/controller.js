/* jshint camelcase: false */
// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
'use strict';

(function() {
  angular
    .module('app.main')
    .controller('MainController', MainController);

  MainController.$inject = ['$state', '$filter', 'Constants', 'DataService'];

  /**
   * Controller initialization
   */
  function MainController($state, $filter, Constants, DataService) {
    var main = this;

    main.photos = [];
    main.search = {};
    main.errorMessage = null;
    main.sortType = 'views';

    main.doSearch = doSearch;
    main.clearErrors = clearErrors;
    main.openGallery = openGallery;
    main.sort = changeSort;

    /**
     * search for photos
     */
    function doSearch() {
      if (!main.search.tag) {
        main.errorMessage = 'Please enter a tag first';
        return false;
      }

      DataService
        .getData({
          method: 'POST',
          url: Constants.get('API_URL'),
          data: {
            tags: main.search.tag,
            user_id: main.search.userId
          }
        })
        .then(parseResponse);
    }

    /**
     * Parse response data
     *
     * @param {object} data
     */
    function parseResponse(data) {
      if (data.stat === 'fail') {
        main.errorMessage = 'An error ocurred: ' + data.message;
      } else {
        if (data.photos.total === '0') {
          main.errorMessage = 'No results matching your search criteria';
        } else {
          data.photos.photo[0].tag = main.search.tag;
          data.photos.photo[0].user_id = main.search.userId;
          main.photos.push(data.photos.photo[0]);
          main.search = {};
        }
      }
    }

    /**
     * Open gallery page
     *
     * @param {object} data
     */
    function openGallery(data) {
      var params = {};

      params.tag = data.tag;

      if (data.user_id) {
        params.userId = data.user_id;
      }

      $state.go('gallery', params);
    }

    /**
     * Empty error message when user focus on input fields
     */
    function clearErrors() {
      main.errorMessage = null;
    }

    function changeSort(field) {
      console.log('sort');
      main.photos = $filter('orderObjectBy')(main.photos, field);
    }
  }
})();
