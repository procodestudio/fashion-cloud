'use strict';

(function() {
  angular
    .module('app.main')
    .component('searchBox', {
      templateUrl: 'templates/search-box.tpl.html',
      bindings: {
        search: '=',
        errorMessage: '=',
        doSearch: '&',
        clearErrors: '&'
      }
    })
    .component('photoCard', {
      templateUrl: 'templates/photo-card.tpl.html',
      bindings: {
        data: '=',
        openGallery: '&'
      }
    })
    .component('customFilters', {
      templateUrl: 'templates/custom-filters.tpl.html',
      bindings: {
        sort: '&'
      }
    });
})();
