'use strict';

(function() {
  angular
    .module('app.main', [])
    .config(mainStateProvider);

  mainStateProvider.$inject = ['$stateProvider'];

  /**
   * Configure module state provider
   */
  function mainStateProvider($stateProvider) {
    $stateProvider
      .state('main', {
        url: '/',
        templateUrl: 'templates/main.tpl.html',
        controller: 'MainController as main'
      });
  }
})();
