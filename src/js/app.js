'use strict';

(function() {
  var modules = [
    'ui.router',
    'app.main',
    'app.gallery'
  ];

  angular
    .module('app', modules)
    .config(appConfig);

  appConfig.$inject = ['$locationProvider', '$urlRouterProvider', '$qProvider'];

  /**
   * Initial config
   */
  function appConfig($locationProvider, $urlRouterProvider, $qProvider) {
    $locationProvider.html5Mode(true);
    $urlRouterProvider.otherwise('/');
    $qProvider.errorOnUnhandledRejections(false);
  }
})();
