'use strict';

(function() {
  angular
    .module('app.gallery')
    .component('galleryList', {
      templateUrl: 'templates/gallery-list.tpl.html',
      bindings: {
        photos: '='
      }
    })
    .component('galleryPagination', {
      templateUrl: 'templates/gallery-pagination.tpl.html',
      bindings: {
        pages: '=',
        current: '=',
        change: '&'
      }
    });
})();
