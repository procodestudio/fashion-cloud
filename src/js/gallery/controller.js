/* jshint camelcase: false */
// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
'use strict';

(function() {
  angular
    .module('app.gallery')
    .controller('GalleryController', GalleryController);

  GalleryController.$inject = ['$state', 'Constants', 'DataService'];

  /**
   * Controller initialization
   */
  function GalleryController($state, Constants, DataService) {
    var gallery = this;

    gallery.currentPage = 1;
    gallery.totalPages = 0;
    gallery.tag = $state.params.tag;
    gallery.owner = $state.params.userId;
    gallery.photos = [];

    gallery.$onInit = init;
    gallery.paginate = paginate;

    /**
     * Init controller
     */
    function init() {
      if (!gallery.tag) {
        $state.go('main');
      } else {
        getPictures();
      }
    }

    /**
     * Get all pictures to create gallery
     */
    function getPictures() {
      DataService
        .getData({
          method: 'POST',
          url: Constants.get('API_URL'),
          data: {
            tags: gallery.tag,
            user_id: gallery.owner,
            per_page: Constants.get('ITEMS_AMOUNT'),
            page: gallery.currentPage
          }
        })
        .then(parseResponse);
    }

    /**
     * Parse response data
     *
     * @param {object} data
     */
    function parseResponse(data) {
      if (data.stat === 'fail') {
        $state.go('main');
      } else {
        if (data.photos.total === '0') {
          $state.go('main');
        } else {
          console.log(data);
          gallery.photos = data.photos.photo;
          gallery.totalPages = data.photos.pages > 10 ? 10 : data.photos.pages;
        }
      }
    }

    /**
     * Paginate gallery
     *
     * @param {int} page
     */
    function paginate(page) {
      gallery.currentPage = page;
      getPictures();
    }
  }
})();
