'use strict';

(function() {
  angular
    .module('app.gallery', [])
    .config(galleryStateProvider);

  galleryStateProvider.$inject = ['$stateProvider'];

  /**
   * Configure module state provider
   */
  function galleryStateProvider($stateProvider) {
    $stateProvider
      .state('gallery', {
        url: '/gallery/:tag/:userId?pagination',
        params: {
          tag: {value: null, squash: true},
          userId: {value: null, squash: false}
        },
        templateUrl: 'templates/gallery.tpl.html',
        controller: 'GalleryController as gallery'
      });
  }
})();
