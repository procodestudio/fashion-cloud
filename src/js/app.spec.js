'use strict';

describe('App start', function() {
  var $locationProvider;
  var $urlRouterProvider;
  var $qProvider;

  beforeEach(function() {
    module('ui.router');
    module(function(_$locationProvider_, _$urlRouterProvider_, _$qProvider_) {
      $locationProvider = _$locationProvider_;
      $urlRouterProvider = _$urlRouterProvider_;
      $qProvider = _$qProvider_;

      spyOn($locationProvider, 'html5Mode');
      spyOn($urlRouterProvider, 'otherwise');
      spyOn($qProvider, 'errorOnUnhandledRejections');
    });
    module('app');
    inject();
  });

  it('should set html5 mode', function() {
    expect($locationProvider.html5Mode).toHaveBeenCalledWith(true);
  });

  it('should set otherwise router', function() {
    expect($urlRouterProvider.otherwise).toHaveBeenCalledWith('/');
  });

  it('should set $q to not throw errors on rejections', function() {
    expect($qProvider.errorOnUnhandledRejections).toHaveBeenCalledWith(false);
  });
});
