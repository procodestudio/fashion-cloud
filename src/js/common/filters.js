'use strict';

(function() {
  angular
    .module('app')
    .filter('orderObjectBy', orderObjectBy)
    .filter('mySQL2date', mySQL2date)
    .filter('ts2date', ts2date);

  /**
   * Order object
   *
   * @returns {Function}
   */
  function orderObjectBy() {
    return function(items, field) {
      var filtered = [];

      angular.forEach(items, function(item) {
        filtered.push(item);
      });

      filtered.sort(function(a, b) {
        if (field === 'views' || field === 'dateupload') {
          return (parseInt(a[field]) < parseInt(b[field]));
        } else {
          return (a[field] < b[field]);
        }
      });

      return filtered;
    };
  }

  /**
   * Convert mySQL date format to humans format
   *
   * @returns {Function}
   */
  function mySQL2date() {
    return function(item) {
      return moment(item).format('DD/MM/YYYY');
    };
  }

  /**
   * Convert timestamp format to humans format
   *
   * @returns {Function}
   */
  function ts2date() {
    return function(item) {
      return moment.unix(parseInt(item)).format('DD/MM/YYYY');
    };
  }
})();
