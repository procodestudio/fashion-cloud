describe('Common factories', function() {
  var Constants;

  beforeEach(function() {
    module('app');
  });

  describe('Constants', function() {
    beforeEach(function() {
      inject(function($injector) {
        Constants = $injector.get('Constants');
      });
    });

    it('should exists', function() {
      expect(Constants).toBeDefined();
    });

    describe('get()', function() {
      it('should exists', function() {
        expect(Constants.get).toBeDefined();
      });

      it('should return a constant', function() {
        expect(Constants.get('TEST')).toEqual('Ok');
      });
    });
  });
});
