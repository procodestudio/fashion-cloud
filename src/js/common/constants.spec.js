describe('Common constants', function() {
  var constants;

  beforeEach(function() {
    module('app');
    inject(function($injector) {
      constants = $injector.get('appConstants');
    });
  });

  it('should exists', function() {
    expect(constants).toBeDefined();
  });
});
