'use strict';

(function() {
  angular
    .module('app')
    .factory('Constants', Constants);

  Constants.$inject = ['appConstants'];

  /**
   * Return a specific constant value
   */
  function Constants(appConstants) {
    return {
      get: getConstant
    };

    /**
     * Return constant value
     */
    function getConstant(key) {
      return appConstants[key];
    }
  }
})();
