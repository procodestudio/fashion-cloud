'use strict';

(function() {
  angular
    .module('app')
    .constant('appConstants', {
      TEST: 'Ok',
      API_URL: 'https://api.flickr.com/services/rest/',
      API_METHOD: 'flickr.photos.search',
      API_KEY: '4639dfaa29e85b22e9cf75dfc922a4f5',
      ITEMS_AMOUNT: 12
    });
})();
