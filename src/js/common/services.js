/* jshint camelcase: false */
// jscs:disable requireCamelCaseOrUpperCaseIdentifiers
'use strict';

(function() {
  angular
    .module('app')
    .service('DataService', DataService);

  DataService.$inject = ['$q', '$http', 'Constants'];

  /**
   * Data service
   */
  function DataService($q, $http, Constants) {
    return {
      getData: getData
    };

    /**
     * Make HTTP requests
     */
    function getData(config) {
      var request;
      var data = !config.data ? {} : config.data;
      var defaultApiConfig;

      defaultApiConfig = {
        method: Constants.get('API_METHOD'),
        api_key: Constants.get('API_KEY'),
        sort: 'interestingness-desc',
        extras: 'date_upload,date_taken,owner_name,views,url_q',
        per_page: data.per_page || 1,
        format: 'json',
        nojsoncallback: 1
      };

      request = $http({
        cache: false,
        method: config.method,
        data: $.param(_.assign(data, defaultApiConfig)),
        url: config.url,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        timeout: 10000
      });

      return request.then(handleSuccess, handleError);
    }

    /**
     * Handle success response from server
     */
    function handleSuccess(response) {
      return response.data;
    }

    /**
     * Handle failed response from server
     */
    function handleError(response) {
      return $q.reject('Error ' + response.status);
    }
  }
})();
