'use strict';

describe('Common services', function() {
  var DataService;
  var $httpBackend;
  var urlSuccess = 'http://localhost/app';
  var urlError = 'http://localhost/app/error';
  var jsonRes = {'id': 12345, 'tag': 'cats'};

  describe('DataService', function() {
    beforeEach(function() {
      module('templates');
      module('app');

      inject(function($injector) {
        DataService = $injector.get('DataService');
        $httpBackend = $injector.get('$httpBackend');
        $httpBackend.when('GET', urlSuccess).respond(200, jsonRes);
        $httpBackend.when('GET', urlError).respond(404);
      });
    });

    afterEach(function() {
      $httpBackend.verifyNoOutstandingExpectation();
      $httpBackend.verifyNoOutstandingRequest();
    });

    it('should exists', function() {
      expect(DataService).toBeDefined();
    });

    describe('getData()', function() {
      it('should exists', function() {
        expect(DataService.getData).toBeDefined();
      });

      it('should return an object when passing data', function() {
        var config = {method: 'GET', url: urlSuccess, data: {tags: 'Cats'}};

        DataService
          .getData(config)
          .then(function(data) {
            expect(typeof data).toBe('object');
          });

        $httpBackend.flush();
      });

      it('should return an object when passing no data', function() {
        var config = {method: 'GET', url: urlSuccess};

        DataService
          .getData(config)
          .then(function(data) {
            expect(typeof data).toBe('object');
          });

        $httpBackend.flush();
      });

      it('should return an error from promise', function() {
        var config = {method: 'GET', url: urlError};

        DataService
          .getData(config)
          .catch(function(error) {
            expect(error).toBe('Error 404');
          });

        $httpBackend.flush();
      });
    });
  });
});
